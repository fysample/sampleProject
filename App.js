/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View
} from 'react-native';

//import App from "./app";

import { StackNavigator } from 'react-navigation';


class mainScreen extends React.Component {

  static navigationOptions = {
    title: 'Welcome guest',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <Text>Hello, lets chat with fify </Text>
        <Button onPress={() => navigate('Chat')}
                title="Chat with fify"
        />
      </View>
    );
  }
}


class chatScreen extends React.Component {
  static navigationOptions = {
    title: 'Chat with fify',
  };

  render () {
    return <Text> hai future senior developer </Text>;
  }
}

const oneScreen = StackNavigator ({
  Home: { screen: mainScreen },
  Chat: { screen: chatScreen},

});


AppRegistry.registerComponent('sampleProject', () => oneScreen);
